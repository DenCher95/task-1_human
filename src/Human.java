public class Human {
    private String lastName;
    private String firstName;
    private String patronymic;

    public Human(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Human(String lastName, String firstName, String patronymic) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
    }

    public void getFullName() {
        if (patronymic == null) {
            System.out.println(lastName + " " + firstName);
        } else {
            System.out.println(lastName + " " + firstName + " " + patronymic);
        }
    }

    public void getShortName() {
        if (patronymic == null) {
            System.out.print(lastName + " " + firstName.charAt(0) + ".");
        } else {
            System.out.print(lastName + " " + firstName.charAt(0) + ". " + patronymic.charAt(0) + ".");
        }
    }
}